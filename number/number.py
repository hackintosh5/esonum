import functools
import inspect
import math
import threading
import typing


class IntDelegateMetaclass(type):
    def __new__(mcs, name: str, bases: tuple[type, ...], attrs: dict[str, typing.Any]):
        for attr in dir(int):
            if attr in ("__new__", "__setattr__", "__setattribute__"):
                continue
            if (len(attr) > 4 and attr.startswith("__") and attr.endswith("__")) \
                    or not (attr.startswith("_") or attr.endswith("_")):
                value = inspect.getattr_static(int, attr)
                if callable(value):
                    attrs.setdefault(attr, get_wrapper(attr, value))
        print(attrs)
        return super().__new__(mcs, name, bases, attrs)

    def __getattr__(self, item):
        print(item)


def get_wrapper(attr, value):
    @functools.wraps(value)
    def wrapper(*args, **kwargs):
        #print(attr, args, kwargs)
        new_args = []
        for arg in args:
            new_args.append(int(arg) if isinstance(arg, Number) else arg)
        for k, v in kwargs.items():
                if isinstance(v, Number):
                    kwargs[k] = int(v)
        return value(*new_args, **kwargs)
    return wrapper


class Number(metaclass=IntDelegateMetaclass):
    one: "Number" = 1
    two: "Number" = 2
    three: "Number" = 3
    four: "Number" = 4
    five: "Number" = 5
    six: "Number" = 6
    seven: "Number" = 7
    eight: "Number" = 8
    nine: "Number" = 9
    ten: "Number" = 10
    eleven: "Number" = 11
    twelve: "Number" = 12
    thirteen: "Number" = 13
    fourteen: "Number" = 14
    fifteen: "Number" = 15
    sixteen: "Number" = 16
    seventeen: "Number" = 17
    eighteen: "Number" = 18
    nineteen: "Number" = 19
    twenty: "Number" = 20
    thirty: "Number" = 30
    forty: "Number" = 40
    fifty: "Number" = 50
    sixty: "Number" = 60
    seventy: "Number" = 70
    eighty: "Number" = 80
    ninety: "Number" = 90
    hundred: "Number" = 100
    thousand: "Number" = 1_000
    million: "Number" = 1_000_000
    billion: "Number" = 1_000_000_000
    trillion: "Number" = 10 ** 12
    quadrillion: "Number" = 10 ** 15
    quintillion: "Number" = 10 ** 18
    sextillion: "Number" = 10 ** 21
    septillion: "Number" = 10 ** 24
    octillion: "Number" = 10 ** 27
    nonillion: "Number" = 10 ** 30
    decillion: "Number" = 10 ** 33
    undecillion: "Number" = 10 ** 36
    duodecillion: "Number" = 10 ** 39
    tredecillion: "Number" = 10 * 42
    quattuordecillion: "Number" = 10 ** 45
    googol: "Number" = 10 ** 100
    centillion: "Number" = 10 ** 303
    a: "Number" = ...

    name_map: dict[str, "Number"] = {}
    num_map: dict[int, "Number"] = {}

    _inited = False

    @classmethod
    def init(cls):
        if cls._inited:
            return cls.name_map
        cls._inited = True
        for name, annotated in typing.get_type_hints(cls).items():
            if not issubclass(annotated, Number):
                continue
            number = getattr(cls, name)
            if isinstance(number, type(Ellipsis)):
                value = NumberValueSingle()
            else:
                assert isinstance(number, int)
                value = _convert_value(number)
            num = Number(value)
            setattr(cls, name, num)
            cls.name_map[name] = num
            cls.num_map[number] = num
        return cls.name_map

    @classmethod
    def annotate(cls):
        return {name: Number for name in cls.name_map}

    PENDING = threading.local()

    def __init__(self, value, *, _composite: bool = False, _right: typing.Optional["Number"] = None):
        super().__init__()
        self.values = value if isinstance(value, list) else [value]
        self._composite = _composite
        self._right = _right
        self._int = None
        if getattr(self.PENDING, "value", None) is None:
            self.PENDING.value = []
            self.PENDING.had_and = False

    def __sub__(self, other) -> "Number":
        if self._int and self._composite:
            _safe_raise(SyntaxError("Storing a number value is forbidden"))
        if isinstance(other, Number):
            ret = Number(self.values + other.values, _composite=True)
            if self._composite:
                self.PENDING.value[-1] = ret
            else:
                self.PENDING.value.append(ret)
            return other._right or ret

    def __getattribute__(self, item):
        ret = super().__getattribute__(item)
        if isinstance(ret, Number) and item[0] != "_":
            if self._int:
                _safe_raise(SyntaxError("Storing a number value is forbidden"))
            if self._composite:
                _safe_raise(SyntaxError("Brackets are forbidden in numbers", self, ret, item))
            # overrule python . stickiness
            return Number(self.values, _right=ret)
        return ret

    def __bool__(self):
        if not self._composite:
            self.PENDING.value.append(self)
        return True

    def __index__(self) -> int:
        return int(self)

    def __int__(self) -> int:
        if not self._composite:
            self.PENDING.value.append(self)
        ret = _convert_int(self.PENDING.value)
        self.PENDING.value.clear()
        self.PENDING.had_and = False
        self._int = ret
        return ret

    def __repr__(self):
        return repr(self.values)


class NumberValue:
    pass


class NumberValueNumber(NumberValue):
    def __init__(self, value: int):
        self.value = value

    def __repr__(self):
        return str(self.value)


class NumberValueSuffix(NumberValue):
    def __init__(self, multiplier: int):
        self.multiplier = multiplier

    def __repr__(self):
        return "*" + str(self.multiplier)


class NumberValueSingle(NumberValue):
    pass


def _convert_value(num: typing.Union[int, type(Ellipsis)]):
    if num is Ellipsis:
        return NumberValueSingle()
    if _power_of_10(num):
        return NumberValueSuffix(num)
    return NumberValueNumber(num)


def _convert_int(values: list[Number]):
    ret = 0
    for part in values:
        ret += _convert_part(part.values)
    return ret


def _convert_part(part: list[NumberValue]):
    last = None
    parsed = 0
    single = False
    for value in part:
        if isinstance(value, NumberValueSuffix):
            if isinstance(last, (NumberValueSuffix, NumberValueNumber)):
                if isinstance(last, NumberValueSuffix) and value.multiplier <= last.multiplier:
                    # e.g. one-thousand-hundred
                    _safe_raise(SyntaxError("suffixes do not go upwards", value, part, last))
                if value.multiplier == 100 and isinstance(last, NumberValueNumber) and _mag(last.value) == 2:
                    # e.g. twenty-hundred
                    _safe_raise(SyntaxError("-hundred cannot be used with 2-digit numbers", value, part))
                parsed *= value.multiplier
            elif isinstance(last, NumberValueSingle):
                parsed = value.multiplier
            else:
                _safe_raise(SyntaxError("suffix at start of part", value, part, last))
        elif isinstance(value, NumberValueSingle):
            if parsed or single:
                _safe_raise(SyntaxError("single quantifier not at start of part", value, part))
            single = True
        elif isinstance(value, NumberValueNumber):
            if single:
                _safe_raise(SyntaxError("single quantifier cannot be used with number", value, part))
            parsed += value.value
        last = value
    return parsed


def _power_of_10(num: int):
    if num < 0:
        _safe_raise(RuntimeError("Negative numbers are not supported", num))
    if num == 1:
        return False
    return math.log10(num).is_integer()


def _mag(num: int):
    if num < 0:
        _safe_raise(RuntimeError("Negative numbers are not supported", num))
    if num < 10:
        return 1
    return math.ceil(math.log10(num))


def _dec_shl(num: int, places: int):
    return num * 10 ** places


def _safe_raise(e: Exception):
    Number.PENDING.value = None
    raise e
